#!/bin/bash

clear

cd ~/projetos/Garoupinha

BLUE='\033[1;34m'
NC='\033[0m'

echo "matando emuladores..."
PID=`ps -eaf | grep emulator | grep -v grep | awk '{print $2}'`

if [[ "" !=  "$PID" ]]; then
  kill -9 $PID
fi

sleep 5


echo "parando node.js..."
PID_NODE=`lsof -ti tcp:8081`

if [[ "" != "$PID_NODE" ]]; then
  kill -9 $PID_NODE
fi 

echo "limpado cache do node..."
node node_modules/react-native/local-cli/cli.js start --reset-cache &>/dev/null &

echo "reiniciando adb server..."
adb kill-server > /dev/null
adb start-server > /dev/null

echo "limpando apk"
rm -f ~/projetos/Garoupinha/android/app/build/outputs/apk/app-debug.apk

EMULATOR_LIST=`emulator -list-avds`
stringarray=($EMULATOR_LIST)

echo "Procurando emuladores..."
EMULATOR=${stringarray[0]} 
if [[ "" != "$EMULATOR"  ]]; then
   echo -e "Emulador encontrado: ${BLUE}$EMULATOR${NC}" 
fi

emulator @$EMULATOR &

sleep 15

react-native run-android
